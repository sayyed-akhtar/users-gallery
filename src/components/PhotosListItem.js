import { GoTrash } from 'react-icons/go';
import { useRemovePhotoMutation } from '../store';

function PhotosListItem({ photo }) {
  const [removePhoto, removePhotoResult] = useRemovePhotoMutation();

  const handleRemovePhoto = () => {
    removePhoto(photo);
  };
  return (
    <div className='relative m-2' onClick={handleRemovePhoto}>
      <img src={photo.url} alt='random album' className='h-20 w-20' />
      <div className='absolute inset-0 flex items-center justify-center hover:bg-gray-200 opacity-0 hover:opacity-70'>
        {removePhotoResult.isLoading ? (
          'Deleting...'
        ) : (
          <GoTrash className='text-3xl' />
        )}
      </div>
    </div>
  );
}

export default PhotosListItem;
