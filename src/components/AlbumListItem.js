import { GoTrash } from 'react-icons/go';
import Button from './Button';
import ExpandablePanel from './ExpandablePanel';
import { useRemoveAlbumMutation } from '../store';

import PhotosList from './PhotosList';

function AlbumListItem({ album }) {
  const [removeAlbum, results] = useRemoveAlbumMutation();
  const handleAlbumDelete = () => {
    removeAlbum(album);
  };

  const header = (
    <div className='flex justify-start items-center'>
      <Button
        className='mr-3'
        loading={results.isLoading}
        onClick={handleAlbumDelete}
      >
        <GoTrash />
      </Button>
      {album.title}
    </div>
  );
  return (
    <ExpandablePanel key={album.id} header={header}>
      <PhotosList album={album} />
    </ExpandablePanel>
  );
}

export default AlbumListItem;
