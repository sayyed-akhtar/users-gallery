import { useFetchAlbumsQuery, useAddAlbumMutation } from '../store';
import AlbumListItem from './AlbumListItem';
import Button from './Button';
import Skeleton from './Skeleton';

function AlbumsList({ user }) {
  const { data, error, isFetching } = useFetchAlbumsQuery(user);

  const [addAlbum, results] = useAddAlbumMutation();

  const hanldeAddAlbum = () => {
    addAlbum(user);
  };

  let content;
  if (isFetching) {
    content = <Skeleton className='h-10 w-full' times={2} />;
  } else if (error) {
    content = <div>Error loading albums.</div>;
  } else {
    content = data.map((album) => (
      <AlbumListItem key={album.id} album={album} />
    ));
  }

  return (
    <div className='p-4'>
      <div className='flex justify-between items-center py-3'>
        <h3 className='text-lg font-semibold'>Albums for {user.name}</h3>
        <Button loading={results.isLoading} onClick={hanldeAddAlbum}>
          + Add Album
        </Button>
      </div>
      <div className=''>{content}</div>
    </div>
  );
}

export default AlbumsList;
