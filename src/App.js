import React from 'react';
import UsersList from './components/UsersList';

function App() {
  return (
    <div className='container mx-auto my-6'>
      <UsersList />
    </div>
  );
}

export default App;
